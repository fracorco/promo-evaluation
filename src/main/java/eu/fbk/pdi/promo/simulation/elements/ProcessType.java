package eu.fbk.pdi.promo.simulation.elements;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.ToString;

@ToString
public class ProcessType {

    private final List<Activity> activities;

    private final Map<String, Integer> emptyPathExecutedActivities;

    public ProcessType(final List<Activity> activities) {

        this.activities = activities;
        this.emptyPathExecutedActivities = new HashMap<String, Integer>();

        for (final Activity a : activities) {
            this.emptyPathExecutedActivities.put(a.getId(), new Integer(0));
        }
    }

    public List<Activity> getActivities() {
        return this.activities;
    }

    public Map<String, Integer> getEmptyPathExecutedActivities() {
        return this.emptyPathExecutedActivities;
    }

}
