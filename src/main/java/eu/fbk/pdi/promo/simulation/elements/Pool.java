package eu.fbk.pdi.promo.simulation.elements;

import java.util.AbstractMap.SimpleEntry;

import com.google.common.collect.Lists;

import eu.fbk.pdi.promo.simulation.ActorInstance;
import eu.fbk.pdi.promo.simulation.Randoms;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(of = "idName")
@ToString
public class Pool implements Comparable<Pool> {

    private final String actorClass;

    private final String idName;

    public Pool(final String actorClass, final String idName) {
        this.actorClass = actorClass;
        this.idName = idName;
    }

    public String getActorClass() {
        return this.actorClass;
    }

    public ActorInstance makeRandom() {

        String rstring;
        if (this.idName.startsWith("date")) {
            rstring = Randoms.randDate();
        } else {
            rstring = Randoms.randUUID();
        }

        return new ActorInstance(this,
                Lists.newArrayList(new SimpleEntry<String, String>(this.idName, rstring)));
    }

    @Override
    public int compareTo(Pool other) {
        return this.idName.compareTo(other.idName);
    }

}
