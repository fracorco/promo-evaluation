package eu.fbk.pdi.promo.triplestore;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.Nullable;

import com.google.common.base.Preconditions;
import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.io.Files;
import com.google.common.io.Resources;

import org.apache.commons.io.FileUtils;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.LinkedHashModel;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFHandler;
import org.eclipse.rdf4j.rio.RDFHandlerException;
import org.eclipse.rdf4j.rio.helpers.StatementCollector;
import org.eclipse.rdf4j.sail.Sail;
import org.eclipse.rdf4j.sail.config.SailFactory;
import org.eclipse.rdf4j.sail.config.SailImplConfig;
import org.eclipse.rdf4j.sail.config.SailRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.fbk.rdfpro.AbstractRDFHandler;
import eu.fbk.rdfpro.AbstractRDFHandlerWrapper;
import eu.fbk.rdfpro.RuleEngine;
import eu.fbk.rdfpro.Ruleset;
import eu.fbk.rdfpro.util.IO;
import eu.fbk.rdfpro.util.QuadModel;
import eu.fbk.rdfpro.util.Statements;

public abstract class InferenceEngine implements AutoCloseable {

    private static final Logger LOGGER = LoggerFactory.getLogger(InferenceEngine.class);

    private static final int MAX_FAILURES = 5;

    private static final ValueFactory VF = SimpleValueFactory.getInstance();

    public static final InferenceEngine NIL = new InferenceEngine() {

        @Override
        public RDFHandler doApply(final RDFHandler sink) {
            return sink;
        }

    };

    private volatile boolean closed = false;

    public static InferenceEngine newGraphDBEngine(@Nullable final Iterable<Statement> schemaStmts,
            @Nullable final String ruleset, final boolean schemaImport) {
        return new GraphDBInferencer(schemaStmts, ruleset, schemaImport);
    }

    public static InferenceEngine newRDFproEngine(@Nullable final Iterable<Statement> schemaStmts,
            final Ruleset ruleset) {
        return new RDFproInferenceEngine(schemaStmts, ruleset);
    }

    public final List<Statement> apply(final Iterable<Statement> stmts) {

        Objects.requireNonNull(stmts);
        Preconditions.checkState(!this.closed);

        final List<Statement> sink = stmts instanceof Collection
                ? Lists.newArrayListWithCapacity(4 * Iterables.size(stmts))
                : Lists.newArrayList();

        int numFailures = 0;
        while (true) {
            try {
                final RDFHandler handler = doApply(new StatementCollector(sink));
                try {
                    handler.startRDF();
                    for (final Statement stmt : stmts) {
                        handler.handleStatement(stmt);
                    }
                    handler.endRDF();
                } finally {
                    IO.closeQuietly(handler);
                }
                return sink;

            } catch (final Throwable ex) {
                ++numFailures;
                if (numFailures >= MAX_FAILURES) {
                    Throwables.throwIfUnchecked(ex);
                    throw new RuntimeException(ex);
                }
                LOGGER.warn("Inference computation failed on attempt " + numFailures + "/"
                        + MAX_FAILURES + ", trying again (error: " + ex.getMessage() + ")");
                sink.clear();
            }
        }
    }

    public final boolean isClosed() {
        return this.closed;
    }

    @Override
    public final synchronized void close() {
        if (!this.closed) {
            this.closed = true;
            doClose();
        }
    }

    abstract RDFHandler doApply(RDFHandler sink);

    void doClose() {
    }

    static final long size(final Repository repository) {
        try (RepositoryConnection connection = repository.getConnection()) {
            try (TupleQueryResult c = connection.prepareTupleQuery( //
                    "SELECT (COUNT(*) AS ?n) { ?s ?p ?o }").evaluate()) {
                return ((Literal) c.next().getValue("n")).longValue();
            }
        }
    }

    private static final class GraphDBInferencer extends InferenceEngine {

        private final List<Statement> schema;

        private final boolean schemaImport;

        private final Set<Statement> blacklist;

        private final String ruleset;

        private final AtomicInteger counter;

        private final List<Repository> created;

        private final List<Repository> free;

        GraphDBInferencer(@Nullable final Iterable<Statement> schemaStmts,
                @Nullable String ruleset, final boolean schemaImport) {

            // Allocate a temporary file for the ruleset, if it is not a file
            if (ruleset == null) {
                ruleset = "empty";
            } else if (ruleset.endsWith(".pie")) {
                try {
                    final URL url = new URL(ruleset);
                    final byte[] bytes = Resources.toByteArray(url);
                    final File tempFile = File.createTempFile("promo-", ".pie");
                    tempFile.deleteOnExit();
                    Files.write(bytes, tempFile);
                    ruleset = tempFile.getAbsolutePath();
                } catch (final MalformedURLException ex) {
                    // Ignore: assume the ruleset is already a file path
                } catch (final IOException ex) {
                    throw new RuntimeException(ex);
                }
            }

            // Initialize object except blacklist of statements not to return as inferences
            this.schema = schemaStmts == null ? null
                    : ImmutableList.copyOf(ImmutableSet.copyOf(schemaStmts));
            this.schemaImport = schemaImport;
            this.ruleset = ruleset;
            this.counter = new AtomicInteger(0);
            this.created = Lists.newArrayList();
            this.free = Lists.newArrayList();

            // Compute blacklisted statements
            final Set<Statement> blacklist = Sets.newHashSet();
            if (this.schema != null) {
                final Repository repository = allocate();
                try (RepositoryConnection connection = repository.getConnection()) {
                    connection.add(this.schema);
                    connection.exportStatements(null, null, null, true, new AbstractRDFHandler() {

                        @Override
                        public void handleStatement(final Statement stmt)
                                throws RDFHandlerException {
                            blacklist.add(Statements.normalize(stmt));
                        }

                    });
                } finally {
                    release(repository);
                }
            }

            // Store blacklisted statements as a member field
            this.blacklist = blacklist.isEmpty() ? null : ImmutableSet.copyOf(blacklist);
        }

        @Override
        RDFHandler doApply(final RDFHandler sink) {

            return new AbstractRDFHandlerWrapper(sink) {

                private Repository repository;

                private RepositoryConnection connection;

                @Override
                public void startRDF() throws RDFHandlerException {
                    super.startRDF();
                    this.repository = allocate();
                    this.connection = this.repository.getConnection();
                    this.connection.begin();
                    if (GraphDBInferencer.this.schema != null
                            && !GraphDBInferencer.this.schemaImport) {
                        this.connection.add(GraphDBInferencer.this.schema);
                    }
                }

                @Override
                public synchronized void handleStatement(final Statement statement)
                        throws RDFHandlerException {
                    this.connection.add(statement);
                }

                @Override
                public void endRDF() throws RDFHandlerException {
                    try {
                        final Set<Statement> blacklist = GraphDBInferencer.this.blacklist;
                        this.connection.commit();
                        this.connection.exportStatements(null, null, null, true,
                                new AbstractRDFHandler() {

                                    @Override
                                    public void handleStatement(final Statement stmt)
                                            throws RDFHandlerException {
                                        if (blacklist == null || !blacklist.contains(stmt)) {
                                            handler.handleStatement(Statements.normalize(stmt));
                                        }
                                    }

                                });
                    } finally {
                        this.connection.close();
                        this.connection = null;
                        release(this.repository);
                        this.repository = null;
                    }
                    super.endRDF();
                }

                @Override
                public void close() {
                    try {
                        if (this.repository != null) {
                            IO.closeQuietly(this.connection);
                            this.connection = null;
                            release(this.repository);
                            this.repository = null;
                        }
                    } finally {
                        super.close();
                    }
                }

            };
        }

        @Override
        synchronized void doClose() {

            // Delete all the repositories in the created list
            for (final Repository repository : this.created) {
                discard(repository);
            }
            this.created.clear();
            this.free.clear();
        }

        private Repository allocate() {

            // Reuse a repository from the pool, if available
            final long ts = System.currentTimeMillis();
            synchronized (this) {
                Preconditions.checkState(!isClosed());
                if (!this.free.isEmpty()) {
                    final Repository repository = this.free.remove(0);
                    if (LOGGER.isDebugEnabled()) {
                        final long size = size(repository);
                        LOGGER.debug("Repository at {} allocated in {} ms, {} tr (reused)",
                                repository.getDataDir(), System.currentTimeMillis() - ts, size);
                    }
                    return repository;
                }
            }

            // Otherwise retrieve factory
            final SailRegistry registry = SailRegistry.getInstance();
            final SailFactory factory = registry.get("graphdb:FreeSail").get();

            // Allocate a temporary directory for the sail
            final File dir = new File(System.getProperty("java.io.tmpdir") + "/graphdb-"
                    + this.counter.incrementAndGet() + "-" + System.currentTimeMillis());

            // Define configuration properties
            final Map<String, String> properties = new HashMap<String, String>();
            properties.put("ruleset", this.ruleset);
            properties.put("noPersist", "true");
            properties.put("storage-folder", dir.getAbsolutePath());
            properties.put("entity-index-size", "100000");
            properties.put("enable-context-index", "true");
            properties.put("in-memory-literal-properties", "true");

            // Store configuration properties in requested RDF graph
            final Model graph = new LinkedHashModel();
            final Resource node = VF.createBNode("graphdb");
            graph.add(node, VF.createIRI("http://www.openrdf.org/config/sail#sailType"),
                    VF.createLiteral("graphdb:FreeSail"));
            for (final Map.Entry<String, String> entry : properties.entrySet()) {
                graph.add(node,
                        VF.createIRI("http://www.ontotext.com/trree/owlim#", entry.getKey()),
                        VF.createLiteral(entry.getValue()));
            }

            // Create the temporary directory and create and initialize the repository
            Repository repository = null;
            try {
                final SailImplConfig config = factory.getConfig();
                config.parse(graph, node);
                final Sail sail = factory.getSail(config);
                sail.setDataDir(dir);
                repository = new SailRepository(sail);
                repository.setDataDir(dir);
                synchronized (this) {
                    Preconditions.checkState(!isClosed());
                    Files.createParentDirs(dir);
                    repository.init();
                    if (this.schema != null && this.schemaImport) {
                        try (RepositoryConnection connection = repository.getConnection()) {
                            connection.begin();
                            connection.add(VF.createBNode(), VF.createIRI(
                                    "http://www.ontotext.com/owlim/system#schemaTransaction"),
                                    VF.createBNode());
                            connection.add(this.schema);
                            connection.commit();
                        }
                    }
                    if (LOGGER.isDebugEnabled()) {
                        final long size = size(repository);
                        LOGGER.debug("Repository at {} allocated in {} ms, {} tr (new)",
                                repository.getDataDir(), System.currentTimeMillis() - ts, size);
                    }
                    this.created.add(repository);
                    return repository;
                }
            } catch (final Throwable ex) {
                if (repository != null) {
                    discard(repository);
                }
                throw new RepositoryException("Could not create GraphDB repository", ex);
            }
        }

        private void release(final Repository repository) {

            try {
                // Clear data in the repository to prepare it for reuse
                final long ts = System.currentTimeMillis();
                try (RepositoryConnection connection = repository.getConnection()) {
                    connection.begin();
                    if (this.schemaImport) {
                        connection.prepareUpdate("DROP DEFAULT").execute();
                    } else {
                        connection.clear();
                    }
                    connection.commit();
                }

                // Log result
                if (LOGGER.isDebugEnabled()) {
                    final long size = size(repository);
                    LOGGER.debug("Repository at {} released in {} ms, {} tr",
                            repository.getDataDir(), System.currentTimeMillis() - ts, size);
                }

                // Return the repository to the pool
                synchronized (this) {
                    Preconditions.checkState(!isClosed());
                    this.free.add(repository);
                }

            } catch (final Throwable ex) {
                // If something goes wrong, drop the repository so a new one will be allocated
                LOGGER.warn("Could not release repository at " + repository.getDataDir()
                        + ", dropping it (error: " + ex.getMessage() + ")");
                discard(repository);
            }
        }

        private void discard(final Repository repository) {

            // Shutdown repository and delete its data directory
            final File dataDir = repository.getDataDir();
            try {
                final long ts = System.currentTimeMillis();
                repository.shutDown();
                if (dataDir != null) {
                    FileUtils.deleteDirectory(dataDir);
                }
                LOGGER.debug("Repository at {} deleted in {} ms", dataDir,
                        System.currentTimeMillis() - ts);
            } catch (final IOException ex) {
                LOGGER.warn("Could not delete repository at " + dataDir + " (error: "
                        + ex.getMessage() + ")");
            }
        }

    }

    private static final class RDFproInferenceEngine extends InferenceEngine {

        private final RuleEngine engine;

        RDFproInferenceEngine(@Nullable final Iterable<Statement> schemaStmts, Ruleset ruleset) {

            Objects.requireNonNull(ruleset);

            if (schemaStmts != null) {
                final QuadModel model = QuadModel.create(schemaStmts);
                ruleset = ruleset.getABoxRuleset(model);
            }

            this.engine = RuleEngine.create(ruleset);
        }

        @Override
        RDFHandler doApply(final RDFHandler sink) {
            return this.engine.eval(sink, false);
        }

    }

}
